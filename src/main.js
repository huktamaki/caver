var colors = {
  black: [0, 0, 0],
  white: [255, 255, 255]
}

var video
var poseNet
var pose
var skeleton

var xStep = 0
var yStep = 0

var states = {
  weapon: 0,
  deer: 0,
  orientation: [1, -1]
}

var posList = [
  [0.1, 0.1],
  [0.1, 0.3],
  [0.1, 0.5],
  [0.1, 0.7],
  [0.3, 0.1],
  [0.3, 0.3],
  [0.3, 0.5],
  [0.3, 0.7],
  [0.5, 0.1],
  [0.5, 0.3],
  [0.5, 0.5],
  [0.5, 0.7],
  [0.7, 0.1],
  [0.7, 0.3],
  [0.7, 0.5],
  [0.7, 0.7]
]

var deers = []
var humans = []

function setup() {
  createCanvas(windowWidth, windowHeight)
  video = createCapture(VIDEO)
  video.hide()
  poseNet = ml5.poseNet(video, modelLoaded)
  poseNet.on('pose', gotPoses)
  posList = shuffle(posList)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  colorMode(RGB, 255, 255, 255, 1)
  rectMode(CENTER)
  background(colors.white)

  if (pose && humans.length <= 7) {
    push()
    translate((windowWidth - video.width) * 0.5, (windowHeight - video.height) * 0.5)
    drawHuman(pose.rightEye.x, pose.rightEye.y, pose.leftEye.x, pose.leftEye.y, pose.rightShoulder.x, pose.rightShoulder.y, pose.leftShoulder.x, pose.leftShoulder.y, pose.rightElbow.x, pose.rightElbow.y, pose.leftElbow.x, pose.leftElbow.y, pose.rightWrist.x, pose.rightWrist.y, pose.leftWrist.x, pose.leftWrist.y, pose.rightHip.x, pose.rightHip.y, pose.leftHip.x, pose.leftHip.y, pose.rightKnee.x, pose.rightKnee.y, pose.leftKnee.x, pose.leftKnee.y, pose.rightAnkle.x, pose.rightAnkle.y, pose.leftAnkle.x, pose.leftAnkle.y, states.weapon, states.orientation[Math.floor(frameCount * 0.04) % 2])
    pop()
  }

  var xNoise = noise(xStep)
  var yNoise = noise(yStep)
  var x = map(xNoise, 0, 1, 0, width)
  var y = map(yNoise, 0, 1, 0, height)

  if (pose && deers.length <= 4) {
    noFill()
    stroke(colors.black)
    strokeWeight(6)
    push()
    translate(windowWidth * 0.4, windowHeight * 0.4)
    drawDeer(dist(pose.rightWrist.x, pose.rightWrist.y, pose.leftWrist.x, pose.leftWrist.y) * 0.5, Math.floor(frameCount * 0.1) % 3, states.orientation[Math.floor(frameCount * 0.04) % 2])
    pop()
  }

  xStep += 0.01
  yStep += 0.005

  // drawing deers
  for (var i = 0; i < deers.length; i++) {
    noFill()
    stroke(colors.black)
    strokeWeight(6)
    push()
    translate(windowWidth * deers[i][0], windowHeight * deers[i][0])
    scale(0.25 + 0.25 * deers[i][5])
    drawDeer(deers[i][2], deers[i][3], deers[i][4])
    pop()
  }
  // drawing humans
  for (var i = 0; i < humans.length; i++) {
    noFill()
    stroke(colors.black)
    strokeWeight(6)
    push()
    translate(windowWidth * humans[i][0], windowHeight * humans[i][1])
    scale(0.25 + 0.25 * humans[i][31])
    drawHuman(humans[i][2], humans[i][3], humans[i][4], humans[i][5], humans[i][6], humans[i][7], humans[i][8], humans[i][9], humans[i][10], humans[i][11], humans[i][12], humans[i][13], humans[i][14], humans[i][15], humans[i][16], humans[i][17], humans[i][18], humans[i][19], humans[i][20], humans[i][21], humans[i][22], humans[i][23], humans[i][24], humans[i][25], humans[i][26], humans[i][27], humans[i][28], humans[i][29], humans[i][30], humans[i][31])
    pop()
  }

  // pushing new deers into array
  if (pose && frameCount % 100 === 0 && deers.length <= 4) {
    deers.push([posList[posList.length - 1][0], posList[posList.length - 1][1], dist(pose.rightWrist.x, pose.rightWrist.y, pose.leftWrist.x, pose.leftWrist.y), Math.floor(Math.random() * 3), states.orientation[Math.floor(Math.random() * 2)], Math.random() * 0.5 + 0.2])
    posList.pop()
  }
  // pushing new humans into array
  if (pose && frameCount % 80 === 0 && humans.length <= 7) {
    humans.push([posList[posList.length - 1][0], posList[posList.length - 1][1], pose.rightEye.x, pose.rightEye.y, pose.leftEye.x, pose.leftEye.y, pose.rightShoulder.x, pose.rightShoulder.y, pose.leftShoulder.x, pose.leftShoulder.y, pose.rightElbow.x, pose.rightElbow.y, pose.leftElbow.x, pose.leftElbow.y, pose.rightWrist.x, pose.rightWrist.y, pose.leftWrist.x, pose.leftWrist.y, pose.rightHip.x, pose.rightHip.y, pose.leftHip.x, pose.leftHip.y, pose.rightKnee.x, pose.rightKnee.y, pose.leftKnee.x, pose.leftKnee.y, pose.rightAnkle.x, pose.rightAnkle.y, pose.leftAnkle.x, pose.leftAnkle.y, states.weapon, Math.random() * 0.5 + 0.2, states.orientation[Math.floor(Math.random() * 2)]])

    states.weapon = Math.floor(Math.random() * 3)
    posList.pop()
  }
}

function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose
    skeleton = poses[0].skeleton
  }
}

function modelLoaded() {
  console.log('poseNet ready')
}

function drawDeer(size, state, orientation) {
  if (state === 0) {
    push()
    if (orientation === -1) {
      translate(size, 0)
    } else {
      translate(0, 0)
    }
    // body
    line(0 * orientation, 0, size * 1.2 * orientation, -size * 0.2)
    line(size * 1.2 * orientation, -size * 0.2, size * 1.1 * orientation, size * 0.5)
    line(size * 1.1 * orientation, size * 0.5, size * 0.1 * orientation, size * 0.5)
    line(size * 0.1 * orientation, size * 0.5, 0 * orientation, 0)
    // legs
    line(size * 1.1 * orientation, size * 0.5, size * 1.2 * orientation, size * 0.7)
    line(size * 1.2 * orientation, size * 0.7, size * 1.1 * orientation, size * 1.1)
    line(size * 1.1 * orientation, size * 0.5, size * 1.1 * orientation, size * 0.6)
    line(size * 1.1 * orientation, size * 0.6, size * 1 * orientation, size * 1.05)
    line(size * 0.1 * orientation, size * 0.5, size * 0.2 * orientation, size * 1.1)
    line(size * 0.1 * orientation, size * 0.5, 0 * orientation, size * 1.05)
    // head
    line(0 * orientation, 0, -size * 0.5 * orientation, size * 0.8)
    line(-size * 0.5 * orientation, size * 0.8, -size * 0.5 * orientation, size * 1.05)
    line(-size * 0.5 * orientation, size * 0.8, -size * 0.55 * orientation, size * 0.65)
    line(-size * 0.5 * orientation, size * 0.8, -size * 0.7 * orientation, size * 0.7)
    // tail
    line(size * 1.2 * orientation, -size * 0.2, size * 1.25 * orientation, size * 0.1)
    pop()
  }
  if (state === 1) {
    push()
    if (orientation === -1) {
      translate(size, 0)
    } else {
      translate(0, 0)
    }
    // body
    line(0 * orientation, 0, size * 1.2 * orientation, -size * 0.1)
    line(size * 1.2 * orientation, -size * 0.1, size * 1.1 * orientation, size * 0.5)
    line(size * 1.1 * orientation, size * 0.5, size * 0.1 * orientation, size * 0.55)
    line(size * 0.1 * orientation, size * 0.55, 0 * orientation, 0)
    // legs
    line(size * 0.1 * orientation, size * 0.55, -size * 0.1 * orientation, size * 1.1)
    line(size * 0.1 * orientation, size * 0.55, size * 0.15 * orientation, size * 1.15)
    line(size * 1.1 * orientation, size * 0.5, size * 1 * orientation, size * 1.1)
    line(size * 1.1 * orientation, size * 0.5, size * 1.3 * orientation, size * 0.6)
    line(size * 1.3 * orientation, size * 0.6, size * 1.2 * orientation, size * 1.15)
    // head
    line(0 * orientation, 0, -size * 0.2 * orientation, -size * 0.3)
    line(-size * 0.2 * orientation, -size * 0.3, -size * 0.45 * orientation, -size * 0.25)
    line(-size * 0.2 * orientation, -size * 0.3, -size * 0.3 * orientation, -size * 0.4)
    line(-size * 0.2 * orientation, -size * 0.3, -size * 0.1 * orientation, -size * 0.35)
    // tail
    line(size * 1.2 * orientation, -size * 0.1, size * 1.3 * orientation, -size * 0.2)
    pop()
  }
  if (state === 2) {
    push()
    if (orientation === -1) {
      translate(size, 0)
    } else {
      translate(0, 0)
    }
    // body
    line(0 * orientation, 0, size * 1.25 * orientation, -size * 0.2)
    line(size * 1.25 * orientation, -size * 0.2, size * 1.15 * orientation, size * 0.4)
    line(size * 1.15 * orientation, size * 0.4, size * 0.2 * orientation, size * 0.4)
    line(size * 0.2 * orientation, size * 0.4, 0 * orientation, 0)
    // legs
    line(size * 1.15 * orientation, size * 0.4, size * 1.8 * orientation, size * 0.65)
    line(size * 1.15 * orientation, size * 0.4, size * 1.85 * orientation, size * 0.5)
    line(size * 0.2 * orientation, size * 0.4, -size * 0.55 * orientation, size * 0.6)
    line(size * 0.2 * orientation, size * 0.4, -size * 0.45 * orientation, size * 0.7)
    // head
    line(0 * orientation, 0, -size * 0.5 * orientation, -size * 0.2)
    line(-size * 0.5 * orientation, -size * 0.2, -size * 0.7 * orientation, -size * 0.1)
    line(-size * 0.5 * orientation, -size * 0.2, -size * 0.6 * orientation, -size * 0.3)
    line(-size * 0.5 * orientation, -size * 0.2, -size * 0.3 * orientation, -size * 0.3)
    // tail
    line(size * 1.25 * orientation, -size * 0.2, size * 1.5 * orientation, -size * 0.3)
    pop()
  }
}

function drawHuman(rEyeX, rEyeY, lEyeX, lEyeY, rShoulderX, rShoulderY, lShoulderX, lShoulderY, rElbowX, rElbowY, lElbowX, lElbowY, rWristX, rWristY, lWristX, lWristY, rHipX, rHipY, lHipX, lHipY, rKneeX, rKneeY, lKneeX, lKneeY, rAnkleX, rAnkleY, lAnkleX, lAnkleY, state, orientation) {
  let eyeR = [rEyeX, rEyeY]
  let eyeL = [lEyeX, lEyeY]
  let d = dist(eyeR[0], eyeR[1], eyeL[0], eyeL[1])
  noFill()
  stroke(colors.black)
  strokeWeight(6)
  ellipse((rEyeX + lEyeX) * 0.5, (rEyeY + lEyeY) * 0.5, d * 2.5)
  if (state === 0) {
    if (orientation === -1) {
      arc(rElbowX, rElbowY, dist((rShoulderX + lShoulderX) * 0.5, (rShoulderY + lShoulderY) * 0.5, (rHipX + lHipX) * 0.5, (rHipY + lHipY) * 0.5) * 1.5, dist((rShoulderX + lShoulderX) * 0.5, (rShoulderY + lShoulderY) * 0.5, (rHipX + lHipX) * 0.5, (rHipY + lHipY) * 0.5) * 2, Math.PI * 0.7, Math.PI * 1.0)
    } else {
      arc(lElbowX, lElbowY, dist((rShoulderX + lShoulderX) * 0.5, (rShoulderY + lShoulderY) * 0.5, (rHipX + lHipX) * 0.5, (rHipY + lHipY) * 0.5) * 1.5, dist((rShoulderX + lShoulderX) * 0.5, (rShoulderY + lShoulderY) * 0.5, (rHipX + lHipX) * 0.5, (rHipY + lHipY) * 0.5) * 2, -Math.PI * 0.3, Math.PI * 0.3)
    }
  }
  if (state === 1) {
    if (orientation === -1) {
      line(lWristX, lWristY, lWristX + 2 * (lElbowY - lWristY), lWristY + 2 * (lElbowY - lWristX))
      line(lWristX, lWristY, lWristX - 2 * (lElbowY - lWristY), lWristY - 2 * (lElbowY - lWristX))
    } else {
      line(rWristX, rWristY, rWristX + 2 * (rElbowY - rWristY), rWristY + 2 * (rElbowY - rWristX))
      line(rWristX, rWristY, rWristX - 2 * (rElbowY - rWristY), rWristY - 2 * (rElbowY - rWristX))
    }
  }
  // upper body
  line(lShoulderX, lShoulderY, rShoulderX, rShoulderY)
  // arms
  line(lShoulderX, lShoulderY, lElbowX, lElbowY)
  line(lWristX, lWristY, lElbowX, lElbowY)
  line(rShoulderX, rShoulderY, rElbowX, rElbowY)
  line(rWristX, rWristY, rElbowX, rElbowY)
  // lower body
  line((rHipX + lHipX) * 0.5, (rHipY + lHipY) * 0.5, (rShoulderX + lShoulderX) * 0.5, (rShoulderY + lShoulderY) * 0.5)
  line(rHipX, rHipY, lHipX, lHipY)
  // legs
  line(rHipX, rHipY, rKneeX, rKneeY)
  line(rAnkleX, rAnkleY, rKneeX, rKneeY)
  line(lHipX, lHipY, lKneeX, lKneeY)
  line(lAnkleX, lAnkleY, lKneeX, lKneeY)
  pop()
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
}
